#include <string.h>
#include <stdio.h>
#include "ft_strstr.c"

int		main(void)
{
	char *res_str;
	char *in1[] = {"12345", "12345", "12345", "12345", "12345", "12345", ""};
	char *in2[] = {"123", "345", "34", "67", "", "3456", "123"};

	for (int i = 0; i < 7; i++)
	{
		char *res = ft_strstr(in1[i], in2[i]);
		char *exp = strstr(in1[i], in2[i]);
		res_str = res == exp ? "Success!" : "Failure!";
		printf("Case: %-7s, %-4s || %s || Exp: %c || Got: %c\n", in1[i], in2[i], res_str, res == 0 ? 'N' : *res, exp == 0 ? 'N' : *exp);
	}
}