/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: wto <wto@student.42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 15:08:43 by wto               #+#    #+#             */
/*   Updated: 2017/08/14 20:15:26 by wto              ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>
#include "ft_strcat.c"

int	main()
{
	char *result_str;
	char s1[10] = {'s', 'u', 'c', '\0', ' ', ' ', ' ', ' '};
	char s1c[10] = {'s', 'u', 'c', '\0', ' ', ' ', ' ', ' '};
	char s1c2[10] = {'s', 'u', 'c', '\0', ' ', ' ', ' ', ' '};
	char s2[5] = {'c', 'e', 's', 's','\0'};
	ft_strcat(s1c, s2);
	strcat(s1c2, s2);
	result_str = strcmp(s1c, s1c2) == 0 ? "Success" : "Failure";
	printf("Case: %-10s || %s || Exp: %-10s || Got: %-10s\n", s1, result_str, s1c2, s1c);
}