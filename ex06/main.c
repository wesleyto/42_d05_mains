/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lberglun <lberglun@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 13:58:59 by lberglun          #+#    #+#             */
/*   Updated: 2017/08/14 19:24:56 by lberglun         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdio.h>
#include "ft_strcmp.c"

int is_negative(int dst1){
	return dst1 < 0;
}

int main()
{
	int dst1;
	int dst2;
	char *res_str;

	char *first_inputs[] = {"hello", "10", "g_gg", "", "", "lukas", "ulkas", "lu", "abcdef"};
	char *second_inputs[] = {"fello", "91", "jjjj", "ljkj", "", "lukas", "lukas", "lukas", "ABCDEF"};
	for (int i = 0; i < 9; i++)
	{
		dst1 = strcmp(first_inputs[i], second_inputs[i]);
		dst2 = ft_strcmp(first_inputs[i], second_inputs[i]);
		res_str = dst1 == dst2 ? "Success!" : "Failure!";
		printf("Input1: %-7s || Input2: %-7s || %s || Exp: %-4d || Got: %-4d\n", first_inputs[i], second_inputs[i], res_str, dst1, dst2);
	}

}