/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lberglun <lberglun@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 15:08:43 by lberglun          #+#    #+#             */
/*   Updated: 2017/08/14 20:15:26 by lberglun         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>
#include "ft_strcapitalize.c"

int	main()
{
	char str[] = "hi my name is lukas";
	char str1[] = "HI MY NAME IS NAME";
	char str2[] = "hi mY Nme is lukas";
	char str3[] = "";
	char str4[] = " ";
	char str5[] = "!@#$abc^&*()";
	char str6[] = "!@#$ABC^&*()";
	char str7[] = "a*B";
	char str8[] = "A*b";
	char str9[] = "a*b";
	char str10[] = "salut, comment tu vas ? 42mots quarante-deux; cinquante+et+un";
	char *strs[] = {str, str1, str2, str3, str4, str5, str6, str7, str8, str9, str10};
	char *exp[] = {
		"Hi My Name Is Lukas", "Hi My Name Is Name", "Hi My Nme Is Lukas", 
		"", " ", "!@#$Abc^&*()", "!@#$Abc^&*()", "A*B", "A*B", "A*B",
		"Salut, Comment Tu Vas ? 42mots Quarante-Deux; Cinquante+Et+Un"};
	char *result_str;
	for (int i = 0; i < 11; i++)
	{
		printf("Case: %-20s", strs[i]);
		ft_strcapitalize(strs[i]);
		result_str = strcmp(strs[i], exp[i]) == 0 ? "Success" : "Failure";
		printf(" || %s || Exp: %-20s || Got: %-20s\n", result_str, exp[i], strs[i]);
	}
	return (0);
}