#include <string.h>
#include <stdio.h>
#include "ft_strncpy.c"

int		main(void)
{
	char *res_str;
	char src[] = "Copy Me";

	char dst1[] = "Failure";
	char dst2[] = "Failure";

	char dst3[] = "Fail This is just filler";
	char dst4[] = "Fail This is just filler";

	char dst5[] = "Fail This is just filler";
	char dst6[] = "Fail This is just filler";

	char dst7[] = "Fail This is just filler";
	char dst8[] = "Fail This is just filler";

	char *strs[] = {dst1, dst2, dst3, dst4, dst5, dst6, dst7, dst8};

	for (int i = 0; i < 8; i += 2)
	{
		strncpy(strs[i], src, 7);
		ft_strncpy(strs[i+1], src, 7);
		res_str = strcmp(strs[i], strs[i+1]) == 0 ? "Success!" : "Failure!";
		printf("Src: %s || %s || Exp: %-25s || Got: %-25s\n", src, res_str, strs[i], strs[i+1]);
	}
}