/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lberglun <lberglun@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 15:08:43 by lberglun          #+#    #+#             */
/*   Updated: 2017/08/14 20:15:26 by lberglun         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>
#include "ft_str_is_numeric.c"

int	main()
{
	char str[] = "01 02 03 04 05";
	char str1[] = "01234567890";
	char str2[] = "01234567890 ";
	char str3[] = "";
	char str4[] = " ";
	char str5[] = "!@#$123^&*()";
	char str6[] = "!@#$^&*()123";
	char str7[] = "1*2";
	char str8[] = "*12";
	char str9[] = "12*";
	char *strs[] = {str, str1, str2, str3, str4, str5, str6, str7, str8, str9};
	int exp[] = {0, 1, 0, 1, 0, 0, 0, 0, 0, 0};
	char *result_str;
	int result;
	for (int i = 0; i < 10; i++)
	{
		result = ft_str_is_numeric(strs[i]);
		result_str = result == exp[i] ? "Success" : "Failure";
		printf("Case: %-20s || %s || Exp: %d || Got: %d\n", strs[i], result_str, exp[i], result);
	}
}