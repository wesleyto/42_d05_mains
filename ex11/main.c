/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lberglun <lberglun@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 15:08:43 by lberglun          #+#    #+#             */
/*   Updated: 2017/08/15 13:05:33 by lberglun         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>
#include "ft_str_is_alpha.c"

int	main()
{
	char str[] = "Hi my name is Lukas";
	char str1[] = "HimynameisLukas";
	char str2[] = "SpaceAtTheEnd ";
	char str3[] = "";
	char str4[] = " ";
	char str5[] = "!@#$abc^&*()";
	char str6[] = "!@#$ABC^&*()";
	char str7[] = "a*B";
	char str8[] = "A*b";
	char str9[] = "a*b";
	char str10[] = "contains1number";
	char *strs[] = {str, str1, str2, str3, str4, str5, str6, str7, str8, str9, str10};
	int exp[] = {0, 1, 0, 1, 0, 0, 0, 0, 0, 0};
	char *result_str;
	int result;
	for (int i = 0; i < 10; i++)
	{
		result = ft_str_is_alpha(strs[i]);
		result_str = result == exp[i] ? "Success" : "Failure";
		printf("Case: %-20s || %s || Exp: %d || Got: %d\n", strs[i], result_str, exp[i], result);
	}
}