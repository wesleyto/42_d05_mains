/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lberglun <lberglun@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 15:08:43 by lberglun          #+#    #+#             */
/*   Updated: 2017/08/14 20:15:26 by lberglun         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>
#include "ft_str_is_printable.c"

int	main()
{
	char str0[] = "\x01\x02\x06\x1F";
	char str1[] = "\x20\x2F\x5F\x7E";
	char str2[] = "Space\x20";
	char str3[] = "";
	char str4[] = " ";
	char str5[] = "P!R@IN#T$A^B&L*E";
	char str6[] = "Heading \x01";
	char str7[] = "\x1F";
	char str8[] = "\x7F\xFF";
	char *strs[] = {str0, str1, str2, str3, str4, str5, str6, str7, str8};
	int exp[] = {0, 1, 1, 1, 1, 1, 0, 0, 0};
	char *result_str;
	int result;
	for (int i = 0; i < 9; i++)
	{
		result = ft_str_is_printable(strs[i]);
		result_str = result == exp[i] ? "Success" : "Failure";
		printf("Case: %-20s || %s || Exp: %d || Got: %d\n", strs[i], result_str, exp[i], result);
	}
}