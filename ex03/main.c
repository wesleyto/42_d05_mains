#include <string.h>
#include <stdio.h>
#include "ft_strcpy.c"

int		main(void)
{
	char *res_str;
	char src[] = "Copy Me";

	char dst1[] = "Failure";
	char dst2[] = "Failure";

	char dst3[] = "Oh, no! This one failed!";
	char dst4[] = "Oh, no! This one failed!";

	strcpy(dst1, src);
	ft_strcpy(dst2, src);
	res_str = strcmp(dst1, dst2) == 0 ? "Success!" : "Failure!";
	printf("Src: %-10s || %s || Exp: %-10s || Got: %-10s\n", src, res_str, dst1, dst2);

	strcpy(dst3, src);
	ft_strcpy(dst4, src);
	res_str = strcmp(dst3, dst4) == 0 ? "Success!" : "Failure!";
	printf("Src: %-10s || %s || Exp: %-10s || Got: %-10s\n", src, res_str, dst3, dst4);
}