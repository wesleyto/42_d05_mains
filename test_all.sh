LAST_EX=16

for num in $(seq -f "%02g" 0 $LAST_EX)
	do
		norminette -R CheckForbiddenSourceHeader ex$num/ft*
	done


for num in $(seq -f "%02g" 0 $LAST_EX)
	do
		gcc -Wall -Wextra -Werror -o ex$num/main ex$num/main.c
	done


for num in $(seq -f "%02g" 0 $LAST_EX)
	do
		echo ===========================
		echo ===== Testing Ex$num =====
		echo ===========================
		./ex$num/main
		echo
	done