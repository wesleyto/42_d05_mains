/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lberglun <lberglun@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 15:08:43 by lberglun          #+#    #+#             */
/*   Updated: 2017/08/14 20:15:26 by lberglun         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>
#include "ft_strlowcase.c"

int	main()
{
	char str[] = "hi my name is lukas";
	char str1[] = "HI MY NAME IS NAME";
	char str2[] = "hi mY Nme is lukas";
	char str3[] = "";
	char str4[] = " ";
	char str5[] = "!@#$abc^&*()";
	char str6[] = "!@#$ABC^&*()";
	char *strs[] = {str, str1, str2, str3, str4, str5, str6};
	char *exp[] = {"hi my name is lukas", "hi my name is name", "hi my nme is lukas", "", " ", "!@#$abc^&*()", "!@#$abc^&*()"};
	char *result_str;
	for (int i = 0; i < 7; i++)
	{
		printf("Case: %-20s", strs[i]);
		ft_strlowcase(strs[i]);
		result_str = strcmp(strs[i], exp[i]) == 0 ? "Success" : "Failure";
		printf(" || %s || Exp: %-20s || Got: %-20s\n", result_str, exp[i], strs[i]);
	}
}