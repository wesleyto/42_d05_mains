#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include "ft_atoi.c"

int		main(void)
{	
	char *cases[] = {
		"0",
		"1",
		"-1",
		"123A123",
		"A123",
		"-A123",
		"2147483647",
		"  123",
		"2147483648",
		"-2147483648",
		"-2147483649",
		"3000000000",
		"-30000000000",
		"+123",
		"++123",
		"+A123",
		"",
		"-70000000000",
		"9223372036854775806",
		"9223372036854775807",
		"9223372036854775808",
		"9223372036854775809",
		"-9223372036854775806",
		"-9223372036854775807",
		"-9223372036854775808",
		"-9223372036854775809",
		"92233720368547758061",
		"-92233720368547758071",
		"\n 2898493",
		"\n\t 289A8493"
	};
	clock_t t;
	int result;
	char *result_str;

	for (int i = 0; i < 30; i++)
	{
		char *test_case = cases[i];
		int exp_result = atoi(test_case);

		t = clock();
		result = ft_atoi(test_case);
		t = clock() - t;

		result_str = exp_result == result ? "Success!" : "Failure!";
		double time_taken = ((double)t)/CLOCKS_PER_SEC; // in seconds

		printf("Case: %-21s || %s || Exp: %-11d || Got: %-11d || Time: %f secs\n", test_case, result_str, exp_result, result, time_taken);
	}
}