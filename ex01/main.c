#include <stdio.h>
#include <unistd.h>
#include "ft_putnbr.c"

int	ft_putchar(char c)
{
	write(1, &c, 1);
	return (0);
}

int		main(void)
{	
	int cases[] = {-2147483648, -2147483647, -25, -1, 0, 1, 25, 2147483646, 2147483647};

	for (int i = 0; i < 9; i++)
	{
		ft_putnbr(cases[i]);
		printf("\t<- got\n%d\t<- exp\n\n", cases[i]);
	}
}