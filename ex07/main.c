/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lberglun <lberglun@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 14:57:25 by lberglun          #+#    #+#             */
/*   Updated: 2017/08/14 19:51:31 by lberglun         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdio.h>
#include "ft_strncmp.c"

int is_negative(dst1){
	return dst1 < 0;
}

int main()
{
	int i;
	int dst1;
	int dst2;
	char *res_str;
	char *first_inputs[] = {"hello", "10", "g_gg", "", "", "lukas", "ulkas", "lu"};
	char *second_inputs[] = {"fello", "91", "jjjj", "ljkj", "", "lukas", "lukas", "lukas"};
	int third_inputs[8] = {0, 3, 2, 2, 0, 3, 5, 0};
	i = 0;
	while (i < 8)
	{
		dst1 = strncmp(first_inputs[i], second_inputs[i], third_inputs[i]);
		dst2 = ft_strncmp(first_inputs[i], second_inputs[i], third_inputs[i]);

		res_str = dst1 == dst2 ? "Success!" : "Failure!";
		printf("Input1: %-7s || Input2: %-7s || Input3: %-2d|| %s || Exp: %-4d || Got: %-4d\n", first_inputs[i], second_inputs[i], third_inputs[i], res_str, dst1, dst2);
		i++;
	}
}