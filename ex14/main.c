/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lberglun <lberglun@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/08/14 15:08:43 by lberglun          #+#    #+#             */
/*   Updated: 2017/08/14 20:15:26 by lberglun         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <string.h>
#include "ft_str_is_uppercase.c"

int	main()
{
	char str[] = "IMALLCAPS";
	char str1[] = "I have LOWERS";
	char str2[] = "SPACEATTHEEND ";
	char str3[] = "";
	char str4[] = " ";
	char str5[] = "!@#$ABC^&*()";
	char str6[] = "!@#$^&*()ABC";
	char str7[] = "A*B";
	char str8[] = "*AB";
	char str9[] = "AB*";
	char str10[] = "A";
	char *strs[] = {str, str1, str2, str3, str4, str5, str6, str7, str8, str9, str10};
	int exp[] = {1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1};
	char *result_str;
	int result;
	for (int i = 0; i < 11; i++)
	{
		result = ft_str_is_uppercase(strs[i]);
		result_str = result == exp[i] ? "Success" : "Failure";
		printf("Case: %-20s || %s || Exp: %d || Got: %d\n", strs[i], result_str, exp[i], result);
	}
}